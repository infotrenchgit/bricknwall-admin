import { Injectable } from '@angular/core';
import { observable, of } from 'rxjs';
import {map, catchError} from 'rxjs/operators';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  _isAuthenicated = false;
  _loggedUserID = null;
  constructor(
    private http:HttpClient
  ) { }

  login(username:String|any,password: String|any){
    const headers = { 'Content-Type': 'application/json' };
    const body = { email: username,password:password,registerType:'normal' };
    return this.http.post(`${environment.API_URL}/userLogin`,body,{headers}).pipe(
      map((res:any)=>{
        if(res.status==1){
          this._isAuthenicated = true;
          this._loggedUserID = res.data._id;
        }else{
          this._isAuthenicated = false;
          this._loggedUserID = null;
        }
        return res;
      }),
      catchError((err:any)=>{
        this._isAuthenicated = false;
        this._loggedUserID = null;
        return of(err.error);
      })
    )
  }

  doLogin(){
    localStorage.setItem('_loggedUserID',this._loggedUserID??'');
  }
  isAuthenticated():boolean{
    if(localStorage.getItem('_loggedUserID')){
      return true;
    }else{
      return false;
    }
  }
  doLogout(){
    this._isAuthenicated = false;
    this._loggedUserID = null;
    localStorage.setItem('_loggedUserID','');
  }
}
