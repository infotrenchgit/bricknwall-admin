import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from 'src/app/guards/auth.guard';
import { ListComponent } from './list/list.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { HomeComponent } from './home/home.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { ConsultantComponent } from './consultant/consultant.component';
import { RealEstateComponent } from './real-estate/real-estate.component';
import { LegalComponent } from './legal/legal.component';

const routes: Routes = [
  {path: 'list',component:ListComponent},
  {path: 'privacy-policy',component:PrivacyPolicyComponent},
  {path: 'home',component:HomeComponent},
  {path: 'about-us',component:AboutUsComponent},
  {path: 'contact-us',component:ContactUsComponent},
  {path: 'consultant',component:ConsultantComponent},
  {path: 'real-estate',component:RealEstateComponent},
  {path: 'legal',component:LegalComponent},


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CmsRoutingModule { }
