import { Component, OnInit } from '@angular/core';
import {Law,initialLaws} from './models/law.model';
import { NgForm } from '@angular/forms';
import { AngularEditorConfig } from '@kolkov/angular-editor';
@Component({
  selector: 'app-legal',
  templateUrl: './legal.component.html',
  styleUrls: ['./legal.component.scss']
})
export class LegalComponent implements OnInit {
  laws:Law[]=initialLaws();
  isEditMode = false;

  aboutUs = {
    id:null,
    title:null,
    description:null,
    icons:[],
    banners:[]
  };

  editorConfig: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    height: '15rem',
    minHeight: '5rem',
    placeholder: 'Enter text here...',
    translate: 'no',
    defaultParagraphSeparator: 'p',
    defaultFontName: 'Arial',
    toolbarHiddenButtons: [
      ['bold']
      ],
    customClasses: [
      {
        name: "quote",
        class: "quote",
      },
      {
        name: 'redText',
        class: 'redText'
      },
      {
        name: "titleText",
        class: "titleText",
        tag: "h1",
      },
    ]
  };

  constructor() { }

  ngOnInit(): void {
  }

  bannerImageUpload(e:any){
    this.aboutUs.banners = e;
  }
  iconImageUpload(e:any){
    this.aboutUs.icons = e;
  }
  update(f:NgForm,errorHTML:HTMLDivElement){}
  updateLaw(){
    this.isEditMode = true;
  }

}
