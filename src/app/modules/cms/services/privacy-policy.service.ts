import { Injectable } from '@angular/core';
import { observable, of, Observable } from 'rxjs';
import {map, catchError, take} from 'rxjs/operators';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PrivacyPolicyService {

  constructor(
    private http:HttpClient
  ) { }

  load(): Observable<any>{
    return this.http.get(`${environment.API_URL}/getPrivacy`).pipe(
      map((_d:any)=>{
        if(_d.status==1){
         let data = _d.data.pop(); 
         _d.data = data;
        }
        return _d;
      })
    );
  }

  update(data:any): Observable<any>{
    const headers = { 'Content-Type': 'application/json' };
    return this.http.post(`${environment.API_URL}/updatePrivacy`,data,{headers});

  }
}
