import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { NgForm } from '@angular/forms';
import { PrivacyPolicyService } from '../services/privacy-policy.service';


@Component({
  selector: 'app-privacy-policy',
  templateUrl: './privacy-policy.component.html',
  styleUrls: ['./privacy-policy.component.scss'],
  providers:[PrivacyPolicyService]
})
export class PrivacyPolicyComponent implements OnInit {
  htmlContent = '';
  bannerImages = [];
  icon=[];
  @ViewChild('errorHTML',{static:true}) errorHTML:ElementRef|undefined;

  privacyPolicy = {
    id:null,
    title:null,
    description:null,
    icons:[],
    banners:[]
  };

  editorConfig: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    height: '15rem',
    minHeight: '5rem',
    placeholder: 'Enter text here...',
    translate: 'no',
    defaultParagraphSeparator: 'p',
    defaultFontName: 'Arial',
    toolbarHiddenButtons: [
      ['bold']
      ],
    customClasses: [
      {
        name: "quote",
        class: "quote",
      },
      {
        name: 'redText',
        class: 'redText'
      },
      {
        name: "titleText",
        class: "titleText",
        tag: "h1",
      },
    ]
  };

  constructor(
    private privacyPolicyServ: PrivacyPolicyService
  ) { }

  ngOnInit(): void {
    this.privacyPolicyServ.load().subscribe((res:any)=>{
      if(res.status ==1){
        this.privacyPolicy.title = res.data.title;
        this.privacyPolicy.description = res.data.description;
        this.privacyPolicy.banners = res.data.banners;
        this.privacyPolicy.icons = res.data.icons;
        this.privacyPolicy.id = res.data._id;
      }


    });
  }

  bannerImageUpload(e:any){
    this.privacyPolicy.banners = e;
  }
  iconImageUpload(e:any){
    this.privacyPolicy.icons = e;
  }

  update(f:NgForm,errorHTML:HTMLDivElement){
    this.privacyPolicyServ.update(this.privacyPolicy).subscribe((res:any)=>{
      errorHTML.innerHTML= res.message;
      errorHTML.classList.remove('hidden');
      if(res.status ==1){
        errorHTML.classList.remove('text-danger');
        errorHTML.classList.add('text-success');
        
      }else{
        errorHTML.classList.add('text-danger');
        errorHTML.classList.remove('text-success');
      }
      errorHTML.scrollIntoView({ behavior: 'smooth', block: 'center' });
    });
  }

}
