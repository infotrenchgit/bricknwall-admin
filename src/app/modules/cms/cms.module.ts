import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {CmsRoutingModule} from './cms-route.module';
import { ListComponent } from './list/list.component';
import { CoreModule } from '../core/core.module';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { FormsModule } from '@angular/forms';
import { HomeComponent } from './home/home.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { ConsultantComponent } from './consultant/consultant.component';
import { RealEstateComponent } from './real-estate/real-estate.component';
import { LegalComponent } from './legal/legal.component';




@NgModule({
  declarations: [ListComponent, PrivacyPolicyComponent, HomeComponent, AboutUsComponent, ContactUsComponent, ConsultantComponent, RealEstateComponent, LegalComponent],
  imports: [
    CommonModule,
    CmsRoutingModule,
    CoreModule,
    FormsModule,
    AngularEditorModule
  ]
})
export class CmsModule { }
