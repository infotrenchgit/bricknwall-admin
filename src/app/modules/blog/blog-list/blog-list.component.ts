import { Component, OnInit } from '@angular/core';
import {Blog, initialBlog} from '../blog.model';
import { BlogService } from '../services/blog.service';
@Component({
  selector: 'app-blog-list',
  templateUrl: './blog-list.component.html',
  styleUrls: ['./blog-list.component.scss']
})
export class BlogListComponent implements OnInit {

  blogs: any;
  error: any;

  constructor(
    private blogService: BlogService
  ) { }

  ngOnInit(): void {
    this.blogService.getBloglist().subscribe((response: any) => {
      if (response && response.status === 1) {
        this.blogs = response.data;
      }
    }, error => {
      this.error = error.message;
    });
  }

}
