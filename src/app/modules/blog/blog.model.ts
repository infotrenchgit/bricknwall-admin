
export type Blog = {
    id:number;
    title:string;
    description:string;
    categories:string[];
    tags?:string[];
    date?:string;
    author:string;
    comments:string[];
}



export function initialBlog<T>():T[]{
    let brokers:T[] = [];
    for(let i=1; i < 10; i ++){
        brokers.push(<T><unknown>{
                id: i,
                title: 'Blog ' + i,
                description: 'This is description',
                categories: ['Category 1','Categorgy2'],
                tags:['Tag1','Tag2'],
                date: new Date(),
                author: 'Himanshu Mahara',
                comments:['Comment1','Comment2']
            });
    }
    return brokers;

}

