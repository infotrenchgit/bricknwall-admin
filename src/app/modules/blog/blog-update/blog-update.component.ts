import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { ActivatedRoute } from '@angular/router';
import { BlogService } from '../services/blog.service';
import { DataService } from '../../shared/service/data-service';
import { Blog } from '../blog.model';
@Component({
  selector: 'app-blog-update',
  templateUrl: './blog-update.component.html',
  styleUrls: ['./blog-update.component.scss']
})
export class BlogUpdateComponent implements OnInit {

  editorConfig: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    height: '15rem',
    minHeight: '5rem',
    placeholder: 'Enter text here...',
    translate: 'no',
    defaultParagraphSeparator: 'p',
    defaultFontName: 'Arial',
    toolbarHiddenButtons: [
      ['bold']
      ],
    customClasses: [
      {
        name: "quote",
        class: "quote",
      },
      {
        name: 'redText',
        class: 'redText'
      },
      {
        name: "titleText",
        class: "titleText",
        tag: "h1",
      },
    ]
  };

  privacyPolicy = {
    id:null,
    title:null,
    description:null,
    icons:[],
    banners:[]
  };

  blog: any;
  imageUrls: any;

  constructor(
    private activeRoute: ActivatedRoute,
    private blogService: BlogService,
    private dataService: DataService
    ) { }

  ngOnInit(): void {
   this.getBlog();
  }

  update(f:NgForm,errorHTML:HTMLDivElement){
    console.log(f);
    this.dataService.sharedParam.subscribe(data => {
      this.imageUrls = data;
    })

    let blog = {
      title: f.value.title,
      description: f.value.description,
      images: this.imageUrls,
      blogId: this.blog._id,
      authorId: this.blog.authorId,
      publishedOn: this.blog.publishedOn,
      isFeatured: f.value.isFeatured
    };

    this.blogService.updateBlog(blog).subscribe((response) => {
      console.log('final response: ', response);
    }, error => {
      console.log('error response: ', error);
    });
  }

  bannerImageUpload(e:any){
    this.privacyPolicy.banners = e;
  }
  iconImageUpload(e:any){
    this.privacyPolicy.icons = e;
  }

  getBlog() {
    this.activeRoute.params.subscribe(parameter => {
      this.blogService.getBlog({ blogId: parameter.id }).subscribe((response: any) => {
        this.blog = response.data.succ;
        this.dataService.changeParam(this.blog.images);
        // this.isLoading = false;
      }, error => {
        // this.isLoading = false;
      });
    })
  }

}
