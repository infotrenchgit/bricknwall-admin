import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BlogRoutingModule } from './blog-routing.module';
import { BlogListComponent } from './blog-list/blog-list.component';
import { BlogUpdateComponent } from './blog-update/blog-update.component';
import { CoreModule } from '../core/core.module';
import { SharedModule } from '../shared/shared.module';
import { FormsModule } from '@angular/forms';
import { AngularEditorModule } from '@kolkov/angular-editor';


@NgModule({
  declarations: [
    BlogListComponent,
    BlogUpdateComponent
  ],
  imports: [
    CommonModule,
    BlogRoutingModule,
    CoreModule,
    SharedModule,
    FormsModule,
    AngularEditorModule
  ]
})
export class BlogModule { }
