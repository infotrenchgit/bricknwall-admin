import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../environments/environment';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

import {Blog} from '../blog.model';

@Injectable({
  providedIn: 'root'
})
export class BlogService {

  constructor( private http:HttpClient) { }

  
  getBloglist() {
    return this.http.get(`${environment.API_URL}/get-blogs`)
  }

  getBlog(blogId: any) {
    return this.http.post(`${environment.API_URL}/getOneBlog`, blogId);
  }

  updateBlog(blog: any) {
    return this.http.post(`${environment.API_URL}/updateBlog`, blog);
  }
  
  getBlogs():Observable<Blog[]>{
    const headers = { 'Content-Type': 'application/json' };
   // const body = { email: username,password:password,registerType:'normal' };
    return this.http.get<Blog[]>(`${environment.API_URL}/get-blogs`).pipe(
      map((res:any)=>res.data)
    );

  }
}
