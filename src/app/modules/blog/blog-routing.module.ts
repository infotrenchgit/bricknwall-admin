import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BlogListComponent } from './blog-list/blog-list.component';
import { BlogUpdateComponent } from './blog-update/blog-update.component';

const routes: Routes = [
  {path:'list',component: BlogListComponent},
  {path:'add',component: BlogUpdateComponent},
  {path:'edit/:id',component: BlogUpdateComponent},

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BlogRoutingModule { }
