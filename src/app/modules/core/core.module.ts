import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutBlankComponent } from './layout-blank/layout-blank.component';
import { LayoutAdminComponent } from './layout-admin/layout-admin.component';
import { SharedModule } from '../shared/shared.module';
import { WidgetFileUploadComponent } from './widget-file-upload/widget-file-upload.component';
import { HttpClientModule } from '@angular/common/http';



@NgModule({
  declarations: [LayoutBlankComponent, LayoutAdminComponent, WidgetFileUploadComponent],
  imports: [
    CommonModule,
    SharedModule
  ],
  exports:[
    HttpClientModule,
    LayoutAdminComponent,
    WidgetFileUploadComponent
  ]
})
export class CoreModule { }
