import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../../environments/environment';
@Injectable({
    providedIn: 'root'
})
export class FileUploadApi {
    constructor(private http: HttpClient){

    }

    upload(file:File,type:String):Observable<any>{
        const formData = new FormData(); 
        
        // Store form name as "file" with file data
        formData.append("files", file, file.name);
        
        // Make http post request over api
        // with formData as req
        return this.http.post(`${environment.API_URL}/upload-file/${type}`, formData)

    }


}
