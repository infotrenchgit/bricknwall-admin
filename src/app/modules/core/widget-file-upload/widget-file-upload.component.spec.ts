import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WidgetFileUploadComponent } from './widget-file-upload.component';

describe('WidgetFileUploadComponent', () => {
  let component: WidgetFileUploadComponent;
  let fixture: ComponentFixture<WidgetFileUploadComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WidgetFileUploadComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WidgetFileUploadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
