import { Component, OnInit, Input, Output,EventEmitter } from '@angular/core';
import { AstMemoryEfficientTransformer } from '@angular/compiler';
import { FileUploadApi } from '../classes/file-upload-api';
 import { DataService } from '../../shared/service/data-service';
@Component({
  selector: 'app-widget-file-upload',
  templateUrl: './widget-file-upload.component.html',
  styleUrls: ['./widget-file-upload.component.scss'],
  providers:[FileUploadApi]
})
export class WidgetFileUploadComponent implements OnInit {
  @Input() limit:any = 1;
  maxLimit = 20;
  @Input() isOverride = false; //not in use
  @Input() files:Array<any>=[];
  tempFiles:Array<any> = [];
  @Input() accept:string = "*";
  @Input() placeholder:string="Upload Files";
  @Input('folder') type = 'default'; 
  @Output() onAfterUpload = new EventEmitter<string[]>();

  imageUrls : string[] = [];

  constructor(private fileUploadApi:FileUploadApi,
    private _dataService: DataService) { }

  ngOnInit(): void {
    if(this.limit > this.maxLimit){
      this.limit = this.maxLimit;
    }
    console.log(this.type);
    this._dataService.sharedParam.subscribe((imageUrls: any) => {
      this.imageUrls = imageUrls;
    });
  }

  
  onChange(event:any) {
    if(this.limit ==1){
      this.files = [];
    }else{
      if((this.files +event.target.files.length)  > this.limit){
        return;
      }
    }
    


    for(let _file of event.target.files){
      const reader = new FileReader();
      reader.onload = (e: any) => {
        let counter = this.tempFiles.length;
        this.tempFiles.push(e.target.result);
        this.fileUploadApi.upload(_file,this.type).subscribe((res:any)=>{
          const r = res;
          this.imageUrls.push(res.data.imageUrl)
          this.files.push(res.data.imageName);
          let data = res;
          this.tempFiles.splice(counter,1);
          this.successUploaded();
        },(err)=>{
        });
      };
      reader.readAsDataURL(_file); 
      
    }
    this._dataService.changeParam(this.imageUrls);
    event.target.value = null;
  }

  successUploaded(){
    this.onAfterUpload.emit(this.files);
  }

}
