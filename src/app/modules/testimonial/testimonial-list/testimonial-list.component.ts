import { Component, OnInit } from '@angular/core';
import {Testimonial, initialTestimonials} from '../testimonial.model';

@Component({
  selector: 'app-testimonial-list',
  templateUrl: './testimonial-list.component.html',
  styleUrls: ['./testimonial-list.component.scss']
})
export class TestimonialListComponent implements OnInit {
  testimonials:Testimonial[] = initialTestimonials();

  constructor() { }

  ngOnInit(): void {
  }

}
