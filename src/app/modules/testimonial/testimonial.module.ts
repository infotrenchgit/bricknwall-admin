import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TestimonialRoutingModule } from './testimonial-routing.module';
import { TestimonialListComponent } from './testimonial-list/testimonial-list.component';
import { TestimonialUpdateComponent } from './testimonial-update/testimonial-update.component';
import { CoreModule } from '../core/core.module';
import { SharedModule } from '../shared/shared.module';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [TestimonialListComponent, TestimonialUpdateComponent],
  imports: [
    CommonModule,
    TestimonialRoutingModule,
    CoreModule,
    SharedModule,
    FormsModule,
    AngularEditorModule

  ]
})
export class TestimonialModule { }
