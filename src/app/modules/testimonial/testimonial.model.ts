
export type Testimonial = {
    id:number;
    name:string;
    title:string;
    photo:string;
    video:string;
    description:string;
}

export function initialTestimonials<T>():T[]{
    let testimonials:T[] = [];
    for(let i=1; i < 10; i ++){
        testimonials.push(<T><unknown>{
                id: i,
                name: 'Jatin Goel ' + i,
                title:'Developer',
                photo:'https://thescpl.org/bricksnwall/assets/images/testimonials/managementspeak_165.jpg',
                video:'',
                description: 'Descroption content',
            });
    }
    return testimonials;

}