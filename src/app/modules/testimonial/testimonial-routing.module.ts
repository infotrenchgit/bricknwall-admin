import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TestimonialListComponent } from './testimonial-list/testimonial-list.component';
import { TestimonialUpdateComponent } from './testimonial-update/testimonial-update.component';

const routes: Routes = [
  {path:'list',component:TestimonialListComponent},
  {path:'add',component:TestimonialUpdateComponent},
  {path:'edit/:id',component:TestimonialUpdateComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TestimonialRoutingModule { }
