import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from 'src/app/guards/auth.guard';
import { BrokerListComponent } from './broker-list/broker-list.component';
import { BrokerUpdateComponent } from './broker-update/broker-update.component';


const routes: Routes = [
  {path: 'list',component:BrokerListComponent},
  {path: 'edit/:id',component:BrokerUpdateComponent},
  {path: 'add',component:BrokerUpdateComponent},


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BrokerRoutingModule { }
