import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-broker-update',
  templateUrl: './broker-update.component.html',
  styleUrls: ['./broker-update.component.scss']
})
export class BrokerUpdateComponent implements OnInit {

  privacyPolicy = {
    id:null,
    title:null,
    description:null,
    icons:[],
    banners:[]
  };

  constructor() { }

  ngOnInit(): void {
  }

  update(f:NgForm,errorHTML:HTMLDivElement){}

  bannerImageUpload(e:any){
    this.privacyPolicy.banners = e;
  }
  iconImageUpload(e:any){
    this.privacyPolicy.icons = e;
  }

}
