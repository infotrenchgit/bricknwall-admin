import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BrokerUpdateComponent } from './broker-update.component';

describe('BrokerUpdateComponent', () => {
  let component: BrokerUpdateComponent;
  let fixture: ComponentFixture<BrokerUpdateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BrokerUpdateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BrokerUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
