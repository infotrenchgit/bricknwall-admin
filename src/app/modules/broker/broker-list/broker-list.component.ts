import { Component, OnInit } from '@angular/core';
import {Broker,initialBrokers} from '../store/broker.model';

@Component({
  selector: 'app-broker-list',
  templateUrl: './broker-list.component.html',
  styleUrls: ['./broker-list.component.scss']
})
export class BrokerListComponent implements OnInit {
  brokers:Broker[] = initialBrokers();

  constructor() { }

  ngOnInit(): void {
  }

}
