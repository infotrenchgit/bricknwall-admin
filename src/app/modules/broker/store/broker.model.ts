
export type Broker = {
    id:number;
    name:string;
    email:string;
    username:string;
    password?:string;
    photo?:string;
}



export function initialBrokers<T>():T[]{
    let brokers:T[] = [];
    for(let i=1; i < 10; i ++){
        brokers.push(<T><unknown>{
                id: i,
                name: 'Broker ' + i,
                email: 'brokertest' + i + '@gmail.com',
                username: 'brokertest' + i,
            });
    }
    return brokers;

}

