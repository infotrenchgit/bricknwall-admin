import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrokerListComponent } from './broker-list/broker-list.component';
import { BrokerUpdateComponent } from './broker-update/broker-update.component';
import { BrokerRoutingModule } from './broker-route.module';
import { CoreModule } from '../core/core.module';
import { SharedModule } from '../shared/shared.module';
import { FormsModule } from '@angular/forms';



@NgModule({
  declarations: [BrokerListComponent, BrokerUpdateComponent],
  imports: [
    CommonModule,
    BrokerRoutingModule,
    CoreModule,
    SharedModule,
    FormsModule
  ]
})
export class BrokerModule { }
