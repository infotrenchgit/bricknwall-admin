import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NriListComponent } from './nri-list/nri-list.component';
import { NriUpdateComponent } from './nri-update/nri-update.component';
import { NriSettingsComponent } from './nri-settings/nri-settings.component';

const routes: Routes = [
  {path:'list',component:NriListComponent},
  {path:'add',component:NriUpdateComponent},
  {path:'edit/:id',component:NriUpdateComponent},
  {path:'settings',component:NriSettingsComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NriRoutingModule { }
