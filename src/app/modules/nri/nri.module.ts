import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NriRoutingModule } from './nri-routing.module';
import { NriSettingsComponent } from './nri-settings/nri-settings.component';
import { NriListComponent } from './nri-list/nri-list.component';
import { NriUpdateComponent } from './nri-update/nri-update.component';
import { CoreModule } from '../core/core.module';
import { SharedModule } from '../shared/shared.module';
import { FormsModule } from '@angular/forms';
import { AngularEditorModule } from '@kolkov/angular-editor';


@NgModule({
  declarations: [NriSettingsComponent, NriListComponent, NriUpdateComponent],
  imports: [
    CommonModule,
    NriRoutingModule,
    CoreModule,
    SharedModule,
    FormsModule,
    AngularEditorModule
  ]
})
export class NriModule { }
