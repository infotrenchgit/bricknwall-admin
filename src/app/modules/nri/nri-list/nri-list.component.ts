import { Component, OnInit } from '@angular/core';
import {Nri,initialNri} from '../nri.model';
@Component({
  selector: 'app-nri-list',
  templateUrl: './nri-list.component.html',
  styleUrls: ['./nri-list.component.scss']
})
export class NriListComponent implements OnInit {
  nriCorners:Nri[] = initialNri();
  constructor() { }

  ngOnInit(): void {
  }

}
