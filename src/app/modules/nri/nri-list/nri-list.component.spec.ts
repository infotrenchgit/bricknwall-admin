import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NriListComponent } from './nri-list.component';

describe('NriListComponent', () => {
  let component: NriListComponent;
  let fixture: ComponentFixture<NriListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NriListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NriListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
