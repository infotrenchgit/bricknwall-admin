import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NriSettingsComponent } from './nri-settings.component';

describe('NriSettingsComponent', () => {
  let component: NriSettingsComponent;
  let fixture: ComponentFixture<NriSettingsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NriSettingsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NriSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
