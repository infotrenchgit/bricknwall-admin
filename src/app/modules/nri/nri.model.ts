
export type Nri = {
    id:number;
    type:string;
    question:string;
    answer:string;
}

export function initialNri<T>():T[]{
    let nriCorners:T[] = [];
    nriCorners.push(<T><unknown>{
        id: 1,
        type: 'non-resident-indian',
        question: 'Who is Non-Resident Indian? ',
        answer:'<p >Section 2(v) of FEMA, 1999 has given definition of "Person resident in India" and from the definition one has to conclude whether he or she is Non-Resident India or not. Following is the definition of Person resident in India.</p>',
    });

    nriCorners.push(<T><unknown>{
        id: 2,
        type: 'non-resident-indian',
        question: 'Who is Person of Indian Origin?',
        answer:'<p >Section 2(v) of FEMA, 1999 has given definition of "Person resident in India" and from the definition one has to conclude whether he or she is Non-Resident India or not. Following is the definition of Person resident in India.</p>',
    });

    nriCorners.push(<T><unknown>{
        id: 3,
        type: 'acquisation-and-transfer-property-in-india',
        question: 'Is it a fact that now Non-Resident Indian and Person of Indian Origin do not require any permission from Reserve Bank to purchase immovable property in India? ',
        answer:'<p >Section 2(v) of FEMA, 1999 has given definition of "Person resident in India" and from the definition one has to conclude whether he or she is Non-Resident India or not. Following is the definition of Person resident in India.</p>',
    });

    nriCorners.push(<T><unknown>{
        id: 4,
        type: 'acquisation-and-transfer-property-in-india',
        question: 'Whether any forms or returns are to be submitted to Reserve Bank after purchase of residential      commercial property under the general permission? ',
        answer:'<p >Section 2(v) of FEMA, 1999 has given definition of "Person resident in India" and from the definition one has to conclude whether he or she is Non-Resident India or not. Following is the definition of Person resident in India.</p>',
    });
    

    return nriCorners;

}