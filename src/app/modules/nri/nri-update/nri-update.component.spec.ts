import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NriUpdateComponent } from './nri-update.component';

describe('NriUpdateComponent', () => {
  let component: NriUpdateComponent;
  let fixture: ComponentFixture<NriUpdateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NriUpdateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NriUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
