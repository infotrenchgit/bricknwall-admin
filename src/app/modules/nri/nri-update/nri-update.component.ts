import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-nri-update',
  templateUrl: './nri-update.component.html',
  styleUrls: ['./nri-update.component.scss']
})
export class NriUpdateComponent implements OnInit {

  editMode = false;
  editorConfig: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    height: '15rem',
    minHeight: '5rem',
    placeholder: 'Enter text here...',
    translate: 'no',
    defaultParagraphSeparator: 'p',
    defaultFontName: 'Arial',
    toolbarHiddenButtons: [
      ['bold']
      ],
    customClasses: [
      {
        name: "quote",
        class: "quote",
      },
      {
        name: 'redText',
        class: 'redText'
      },
      {
        name: "titleText",
        class: "titleText",
        tag: "h1",
      },
    ]
  };

  privacyPolicy = {
    id:null,
    title:null,
    description:null,
    icons:[],
    banners:[]
  };

  constructor(
    public route:ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.route.params.subscribe((res)=>{
      if(+res.id){
        this.editMode = true;

      }  
    });
  }

  update(f:NgForm,errorHTML:HTMLDivElement){}

  bannerImageUpload(e:any){
    this.privacyPolicy.banners = e;
  }
  iconImageUpload(e:any){
    this.privacyPolicy.icons = e;
  }

}
