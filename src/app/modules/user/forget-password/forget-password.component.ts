import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-forget-password',
  templateUrl: './forget-password.component.html'
})
export class ForgetPasswordComponent implements OnInit {

  forgetPasswordForm: FormGroup= this.fb.group({
    email: ['', [Validators.required, Validators.email]],
  });
  submitted = false;

  constructor(private fb: FormBuilder,) { }

  ngOnInit(): void {
  }

  onSubmit () {
    this.submitted = true;
    if (this.forgetPasswordForm.valid) {
      alert('Form Submitted succesfully!!!\n Check the values in browser console.');
      console.table(this.forgetPasswordForm.value);
    }
  }

  get registerFormControl() {
    return this.forgetPasswordForm.controls;
  }
}
