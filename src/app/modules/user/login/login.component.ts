import { Component, OnInit } from '@angular/core';
import {NgForm} from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import {Router, ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm = {
    username:null,
    password:null
  }
  loginResponseError = '';

  constructor(
    private auth: AuthService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    if(this.auth.isAuthenticated()){
      this.router.navigate(['../dashboard'],{'relativeTo':this.route});
    }
  }
  
  loginSubmit(f:NgForm){
    console.log(f);
    this.auth.login(this.loginForm.username,this.loginForm.password).subscribe((res:any)=>{
      if(res.status==1){
        this.auth.doLogin();
        this.router.navigate(['../dashboard'],{'relativeTo':this.route});
      }else{
        this.loginResponseError = res.message;
      }
    });
    

  }

}
