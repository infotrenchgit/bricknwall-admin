import { Component, OnInit, OnDestroy } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { User } from '../store/user.model';
import { UserService } from '../services/user.service';
import { Subscription } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.scss']
})
export class EditUserComponent implements OnInit, OnDestroy {
  privacyPolicy = {
    id:null,
    title:null,
    description:null,
    icons:[],
    banners:[]
  };

  userForm = {
    id:'',
    username:'',
    name:'',
    email:'',
    image:''
  };
  user!:User;
  userSubscription = new Subscription();

  constructor(
    private route:ActivatedRoute,
    private userServ:UserService

    ) { }

  ngOnInit(): void {
    this.route.params.subscribe((res:any)=>{
      this.userSubscription = this.userServ.getUser(res.id).subscribe((user:User)=>{
        console.log(user);
        this.userForm.id = user._id;
        this.userForm.username = user.username;
        this.userForm.name = user.fullName;
        this.userForm.email = user.email;


      });
    });
  }
  update(f:NgForm,errorHTML:HTMLDivElement){}

  bannerImageUpload(e:any){
    this.privacyPolicy.banners = e;
  }
  iconImageUpload(e:any){
    this.privacyPolicy.icons = e;
  }
  ngOnDestroy(){
    this.userSubscription.unsubscribe();

  }

}
