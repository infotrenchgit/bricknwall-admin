import { Injectable } from '@angular/core';
import {environment} from '../../../../environments/environment';

import {User} from '../store/user.model';
import { of,Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map, tap } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http:HttpClient) { }

  getUsers(search:string,page:number,limit:number): Observable<User[]> {
    return this.http.get<User[]>(`${environment.API_URL}/getAllUsers?page=${page}&limit=${limit}`)
    .pipe(map((res:any)=>{
      if(res.data !== undefined){
        return res.data;
      }
    }));
  }

  getUser(id:string): Observable<User>{
    return this.http.post<User>(`${environment.API_URL}/getUserProfileAdmin`,{userId:id})
    .pipe(map((res:any)=>{
      if(res.data !== undefined){
        return res.data;
      }
    }));

  }
}
