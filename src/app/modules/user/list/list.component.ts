import { Component, OnInit } from '@angular/core';
import {User} from '../store/user.model';
import { UserService } from '../services/user.service';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  //users: User[] = initialUser();
  inProgress = true;
  users$!: Observable<User[]>;
  constructor(private userServ:UserService) { }

  ngOnInit(): void {
    this.users$ = this.getUsers('',1,2);
    this.users$.subscribe((res)=>console.log(res));
  }

  search(input:HTMLInputElement){
    this.users$ =  this.getUsers('',1,2);
  }

  getUsers(search = '',page:number,limit = 20){
    this.inProgress = true;
    return this.userServ.getUsers(search,page,limit).pipe(tap(()=>this.inProgress = false));

  }

}
