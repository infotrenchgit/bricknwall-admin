import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ListComponent } from './list/list.component';
import { LogoutComponent } from './logout/logout.component';
import { AuthGuard } from 'src/app/guards/auth.guard';
import { EditUserComponent } from './edit-user/edit-user.component';
import { ForgetPasswordComponent } from './forget-password/forget-password.component';

const routes: Routes = [
  {path: 'login',component:LoginComponent},
  {path: 'dashboard',component:DashboardComponent,canActivate:[AuthGuard]},
  {path: 'list',component:ListComponent},
  {path: 'edit/:id',component:EditUserComponent},
  {path: 'add',component:EditUserComponent},
  {path: 'logout',component:LogoutComponent},
  {path: 'forget-password', component: ForgetPasswordComponent}

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
