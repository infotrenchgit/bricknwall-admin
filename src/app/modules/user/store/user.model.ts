// export class User{
//     constructor(
//         public id:number,
//         public name: string,
//         public email:string,
//         private _password?:string,
//     ){}
// }

export type User= {
    _id:string;
    fullName:string;
    username:string;
    email:string;
    mobile:string;
    isActive:boolean;
    userStatus:string;
    password?:string;


}

// export function initialUser(){
//     let users:User[] = [];
//     for(let i=1; i < 10; i ++){
//         users.push(new User(i,'User '+ i,'usertest0'+1+'@gmail.com'));
//     }
//     return users;

// }