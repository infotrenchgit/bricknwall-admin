import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TeamListComponent } from './team-list/team-list.component';
import { TeamUpdateComponent } from './team-update/team-update.component';

const routes: Routes = [
  {path:'list',component:TeamListComponent},
  {path:'add',component:TeamUpdateComponent},
  {path:'edit/:id',component:TeamUpdateComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TeamRoutingModule { }
