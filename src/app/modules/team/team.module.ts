import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TeamRoutingModule } from './team-routing.module';
import { TeamListComponent } from './team-list/team-list.component';
import { TeamUpdateComponent } from './team-update/team-update.component';
import { CoreModule } from '../core/core.module';
import { SharedModule } from '../shared/shared.module';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [TeamListComponent, TeamUpdateComponent],
  imports: [
    CommonModule,
    TeamRoutingModule,
    CoreModule,
    SharedModule,
    FormsModule,
    AngularEditorModule

  ]
})
export class TeamModule { }
