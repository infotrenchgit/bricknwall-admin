import { Component, OnInit } from '@angular/core';
import {Team, initialTeam} from '../team.model';

@Component({
  selector: 'app-team-list',
  templateUrl: './team-list.component.html',
  styleUrls: ['./team-list.component.scss']
})
export class TeamListComponent implements OnInit {
  teams:Team[] = initialTeam();

  constructor() { }

  ngOnInit(): void {
  }

}
