
export type Team = {
    id:number;
    name:string;
    photo:string;
    designation:string;
    description:string;
}

export function initialTeam<T>():T[]{
    let teams:T[] = [];
    teams.push(<T><unknown>{
        id: 1,
        name: 'Team ' + 1,
        description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Laboriosam, reiciendis. ',
        photo: 'http://thescpl.org/bricksnwall/assets/images/successstory/Mohitmishra.png',
        designation:'Web Developer',
    });
    teams.push(<T><unknown>{
        id: 2,
        name: 'Team ' + 2,
        description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Laboriosam, reiciendis. ',
        photo: 'http://thescpl.org/bricksnwall/assets/images/successstory/Mohitmishra.png',
        designation:'Web Designer',
    });
    teams.push(<T><unknown>{
        id: 3,
        name: 'Team ' + 3,
        description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Laboriosam, reiciendis. ',
        photo: 'http://thescpl.org/bricksnwall/assets/images/successstory/Mohitmishra.png',
        designation:'Content Writer',
    });
    teams.push(<T><unknown>{
        id: 4,
        name: 'Team ' + 4,
        description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Laboriosam, reiciendis. ',
        photo: 'http://thescpl.org/bricksnwall/assets/images/successstory/Mohitmishra.png',
        designation:'Manager',
    });
    teams.push(<T><unknown>{
        id: 5,
        name: 'Team ' + 5,
        description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Laboriosam, reiciendis. ',
        photo: 'http://thescpl.org/bricksnwall/assets/images/successstory/Mohitmishra.png',
        designation:'Project Lead',
    });

    return teams;

}