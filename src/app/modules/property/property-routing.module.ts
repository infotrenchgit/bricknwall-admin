import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PropertyListComponent } from './property-list/property-list.component';
import {PropertyUpdateComponent} from './property-update/property-update.component';


const routes: Routes = [
  {path: 'list',component:PropertyListComponent},
  {path: 'add',component:PropertyUpdateComponent},
  {path: 'edit/:id',component:PropertyUpdateComponent},
  
  

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PropertyRoutingModule { }
