import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import {Property,propertyData} from '../property.modal';
import { ActivatedRoute, Data } from '@angular/router';

@Component({
  selector: 'app-property-update',
  templateUrl: './property-update.component.html',
  styleUrls: ['./property-update.component.scss']
})
export class PropertyUpdateComponent implements OnInit {
  isEditMode = false;
  property:Property = {
    id:0,
    title:'',
    description:'',
    location:'',
    images:[],
    isFeature:false,
    type:1,
    publishDate: new Date()
  };

  editorConfig: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    height: '15rem',
    minHeight: '5rem',
    placeholder: 'Enter text here...',
    translate: 'no',
    defaultParagraphSeparator: 'p',
    defaultFontName: 'Arial',
    toolbarHiddenButtons: [
      ['bold']
      ],
    customClasses: [
      {
        name: "quote",
        class: "quote",
      },
      {
        name: 'redText',
        class: 'redText'
      },
      {
        name: "titleText",
        class: "titleText",
        tag: "h1",
      },
    ]
  };


  constructor(
    private route:ActivatedRoute
  ) { }

  ngOnInit(): void {
    if(this.route.snapshot.params['id'] !== undefined){
      const id = this.route.snapshot.params['id'];
      this.property = propertyData[id];
    }
    
    this.route.params.subscribe((d:Data)=>{
      if(propertyData[d.id] !== undefined){
        this.isEditMode = true;
        this.property = propertyData[d.id];

      }
      
      
    });
  }

  update(f:NgForm,errorElement:HTMLDivElement){

  }
  onPhotoUpload(e:any){

  }

}
