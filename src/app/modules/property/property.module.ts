import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PropertyListComponent } from './property-list/property-list.component';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { CoreModule } from '../core/core.module';
import { PropertyRoutingModule } from './property-routing.module';
import { PropertyUpdateComponent } from './property-update/property-update.component';
import { AngularEditorModule } from '@kolkov/angular-editor';



@NgModule({
  declarations: [PropertyListComponent, PropertyUpdateComponent],
  imports: [
    CommonModule,
    PropertyRoutingModule,
    FormsModule,
    SharedModule,
    CoreModule,
    AngularEditorModule
  ]
})
export class PropertyModule { }
