
export interface Property{
    id:number;
    title: String;
    description:string;
    images:String[];
    location:string;
    isFeature:boolean;
    type:number;
    publishDate:Date;
    //owner:PropertyOwner;

};

export interface PropertyOwner{
    id: number;
    name: String;
    bio:String;
    hobbies:String;
    photo:String;
    reviews: String[];
    facebook:String;
    twitter:String;
    linkedin:String;
    youtube:String;


    
}

export interface PropertyComment{

}



export class PropertyModel{

    constructor(public property: Property){

    }

}
export const propertyData:Property[] = [
    {
        id:1,
        title: 'Diamond Manor Apartment',
        description: 'Massa tempor nec feugiat nisl pretium. Egestas fringilla phasellus faucibus scelerisque eleifend donec Porta nibh venenatis cras sed felis eget velit aliquet. Neque volutpat ac tincidunt vitae semper quis lectus. Turpis in eu mi bibendum neque egestas congue quisque. Sed elementum tempus egestas sed sed risus pretium quam. Dignissim sodales ut eu sem. Nibh mauris cursus m',
        images:['https://maelectrics.com/tf/html/quarter-preview/quarter/img/img-slide/32.jpg','https://maelectrics.com/tf/html/quarter-preview/quarter/img/img-slide/31.jpg','https://maelectrics.com/tf/html/quarter-preview/quarter/img/img-slide/35.jpg','https://maelectrics.com/tf/html/quarter-preview/quarter/img/img-slide/34.jpg','https://maelectrics.com/tf/html/quarter-preview/quarter/img/img-slide/33.jpg'],
        location:'Belmont Gardens, Chicago',
        isFeature:true,
        type:1,
        publishDate: new Date()
    },
    {
        id:2,
        title: 'Diamond Manor Apartment 2',
        description: 'Massa tempor nec feugiat nisl pretium. Egestas fringilla phasellus faucibus scelerisque eleifend donec Porta nibh venenatis cras sed felis eget velit aliquet. Neque volutpat ac tincidunt vitae semper quis lectus. Turpis in eu mi bibendum neque egestas congue quisque. Sed elementum tempus egestas sed sed risus pretium quam. Dignissim sodales ut eu sem. Nibh mauris cursus m',
        images:['https://maelectrics.com/tf/html/quarter-preview/quarter/img/img-slide/32.jpg','https://maelectrics.com/tf/html/quarter-preview/quarter/img/img-slide/31.jpg','https://maelectrics.com/tf/html/quarter-preview/quarter/img/img-slide/35.jpg','https://maelectrics.com/tf/html/quarter-preview/quarter/img/img-slide/34.jpg','https://maelectrics.com/tf/html/quarter-preview/quarter/img/img-slide/33.jpg'],
        location:'Belmont Gardens, Chicago',
        isFeature:false,
        type:1,
        publishDate: new Date()
    },
    {
        id:3,
        title: 'Diamond Manor Apartment',
        description: 'Massa tempor nec feugiat nisl pretium. Egestas fringilla phasellus faucibus scelerisque eleifend donec Porta nibh venenatis cras sed felis eget velit aliquet. Neque volutpat ac tincidunt vitae semper quis lectus. Turpis in eu mi bibendum neque egestas congue quisque. Sed elementum tempus egestas sed sed risus pretium quam. Dignissim sodales ut eu sem. Nibh mauris cursus m',
        images:['https://maelectrics.com/tf/html/quarter-preview/quarter/img/img-slide/32.jpg','https://maelectrics.com/tf/html/quarter-preview/quarter/img/img-slide/31.jpg','https://maelectrics.com/tf/html/quarter-preview/quarter/img/img-slide/35.jpg','https://maelectrics.com/tf/html/quarter-preview/quarter/img/img-slide/34.jpg','https://maelectrics.com/tf/html/quarter-preview/quarter/img/img-slide/33.jpg'],
        location:'Belmont Gardens, Chicago',
        isFeature:true,
        type:1,
        publishDate: new Date()
    },
    {
        id:4,
        title: 'Diamond Manor Apartment',
        description: 'Massa tempor nec feugiat nisl pretium. Egestas fringilla phasellus faucibus scelerisque eleifend donec Porta nibh venenatis cras sed felis eget velit aliquet. Neque volutpat ac tincidunt vitae semper quis lectus. Turpis in eu mi bibendum neque egestas congue quisque. Sed elementum tempus egestas sed sed risus pretium quam. Dignissim sodales ut eu sem. Nibh mauris cursus m',
        images:['https://maelectrics.com/tf/html/quarter-preview/quarter/img/img-slide/32.jpg','https://maelectrics.com/tf/html/quarter-preview/quarter/img/img-slide/31.jpg','https://maelectrics.com/tf/html/quarter-preview/quarter/img/img-slide/35.jpg','https://maelectrics.com/tf/html/quarter-preview/quarter/img/img-slide/34.jpg','https://maelectrics.com/tf/html/quarter-preview/quarter/img/img-slide/33.jpg'],
        location:'Belmont Gardens, Chicago',
        isFeature:true,
        type:1,
        publishDate: new Date()
    },
    {
        id:5,
        title: 'Diamond Manor Apartment',
        description: 'Massa tempor nec feugiat nisl pretium. Egestas fringilla phasellus faucibus scelerisque eleifend donec Porta nibh venenatis cras sed felis eget velit aliquet. Neque volutpat ac tincidunt vitae semper quis lectus. Turpis in eu mi bibendum neque egestas congue quisque. Sed elementum tempus egestas sed sed risus pretium quam. Dignissim sodales ut eu sem. Nibh mauris cursus m',
        images:['https://maelectrics.com/tf/html/quarter-preview/quarter/img/img-slide/32.jpg','https://maelectrics.com/tf/html/quarter-preview/quarter/img/img-slide/31.jpg','https://maelectrics.com/tf/html/quarter-preview/quarter/img/img-slide/35.jpg','https://maelectrics.com/tf/html/quarter-preview/quarter/img/img-slide/34.jpg','https://maelectrics.com/tf/html/quarter-preview/quarter/img/img-slide/33.jpg'],
        location:'Belmont Gardens, Chicago',
        isFeature:true,
        type:1,
        publishDate: new Date()
    },
    {
        id:6,
        title: 'Diamond Manor Apartment',
        description: 'Massa tempor nec feugiat nisl pretium. Egestas fringilla phasellus faucibus scelerisque eleifend donec Porta nibh venenatis cras sed felis eget velit aliquet. Neque volutpat ac tincidunt vitae semper quis lectus. Turpis in eu mi bibendum neque egestas congue quisque. Sed elementum tempus egestas sed sed risus pretium quam. Dignissim sodales ut eu sem. Nibh mauris cursus m',
        images:['https://maelectrics.com/tf/html/quarter-preview/quarter/img/img-slide/32.jpg','https://maelectrics.com/tf/html/quarter-preview/quarter/img/img-slide/31.jpg','https://maelectrics.com/tf/html/quarter-preview/quarter/img/img-slide/35.jpg','https://maelectrics.com/tf/html/quarter-preview/quarter/img/img-slide/34.jpg','https://maelectrics.com/tf/html/quarter-preview/quarter/img/img-slide/33.jpg'],
        location:'Belmont Gardens, Chicago',
        isFeature:true,
        type:1,
        publishDate: new Date()
    },
    {
        id:7,
        title: 'Diamond Manor Apartment',
        description: 'Massa tempor nec feugiat nisl pretium. Egestas fringilla phasellus faucibus scelerisque eleifend donec Porta nibh venenatis cras sed felis eget velit aliquet. Neque volutpat ac tincidunt vitae semper quis lectus. Turpis in eu mi bibendum neque egestas congue quisque. Sed elementum tempus egestas sed sed risus pretium quam. Dignissim sodales ut eu sem. Nibh mauris cursus m',
        images:['https://maelectrics.com/tf/html/quarter-preview/quarter/img/img-slide/32.jpg','https://maelectrics.com/tf/html/quarter-preview/quarter/img/img-slide/31.jpg','https://maelectrics.com/tf/html/quarter-preview/quarter/img/img-slide/35.jpg','https://maelectrics.com/tf/html/quarter-preview/quarter/img/img-slide/34.jpg','https://maelectrics.com/tf/html/quarter-preview/quarter/img/img-slide/33.jpg'],
        location:'Belmont Gardens, Chicago',
        isFeature:true,
        type:1,
        publishDate: new Date()
    },
    {
        id:8,
        title: 'Diamond Manor Apartment',
        description: 'Massa tempor nec feugiat nisl pretium. Egestas fringilla phasellus faucibus scelerisque eleifend donec Porta nibh venenatis cras sed felis eget velit aliquet. Neque volutpat ac tincidunt vitae semper quis lectus. Turpis in eu mi bibendum neque egestas congue quisque. Sed elementum tempus egestas sed sed risus pretium quam. Dignissim sodales ut eu sem. Nibh mauris cursus m',
        images:['https://maelectrics.com/tf/html/quarter-preview/quarter/img/img-slide/32.jpg','https://maelectrics.com/tf/html/quarter-preview/quarter/img/img-slide/31.jpg','https://maelectrics.com/tf/html/quarter-preview/quarter/img/img-slide/35.jpg','https://maelectrics.com/tf/html/quarter-preview/quarter/img/img-slide/34.jpg','https://maelectrics.com/tf/html/quarter-preview/quarter/img/img-slide/33.jpg'],
        location:'Belmont Gardens, Chicago',
        isFeature:true,
        type:1,
        publishDate: new Date()
    },



];

