import { Component, OnInit } from '@angular/core';
import {Property,propertyData, PropertyModel} from '../property.modal';

@Component({
  selector: 'app-property-list',
  templateUrl: './property-list.component.html',
  styleUrls: ['./property-list.component.scss']
})
export class PropertyListComponent implements OnInit {
  properties:Array<Property> = propertyData;

  constructor() { }

  ngOnInit(): void {
  }

}
