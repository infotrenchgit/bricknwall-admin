import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FaqRoutingModule } from './faq-routing.module';
import { FaqListComponent } from './faq-list/faq-list.component';
import { FaqUpdateComponent } from './faq-update/faq-update.component';
import { CoreModule } from '../core/core.module';
import { SharedModule } from '../shared/shared.module';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { FormsModule } from '@angular/forms';
import { FaqSettingsComponent } from './faq-settings/faq-settings.component';


@NgModule({
  declarations: [FaqListComponent, FaqUpdateComponent, FaqSettingsComponent],
  imports: [
    CommonModule,
    FaqRoutingModule,
    CoreModule,
    SharedModule,
    FormsModule,
    AngularEditorModule
  ]
})
export class FaqModule { }
