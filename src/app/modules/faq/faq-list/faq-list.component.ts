import { Component, OnInit } from '@angular/core';
import {Faq, initialFaq} from '../faq.model';


@Component({
  selector: 'app-faq-list',
  templateUrl: './faq-list.component.html',
  styleUrls: ['./faq-list.component.scss']
})
export class FaqListComponent implements OnInit {
  faqs:Faq[] = initialFaq();

  constructor() {}

  ngOnInit(): void {
  }

}
