import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FaqListComponent } from './faq-list/faq-list.component';
import { FaqUpdateComponent } from './faq-update/faq-update.component';
import { FaqSettingsComponent } from './faq-settings/faq-settings.component';

const routes: Routes = [
  {path:'list',component:FaqListComponent},
  {path:'add',component:FaqUpdateComponent},
  {path:'settings',component:FaqSettingsComponent},
  {path:'edit/:id',component:FaqUpdateComponent}

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FaqRoutingModule { }
