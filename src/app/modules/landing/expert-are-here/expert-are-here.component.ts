import { Component, OnInit } from '@angular/core';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import {Expert,initialExperts} from '../models/expert.model';

@Component({
  selector: 'app-expert-are-here',
  templateUrl: './expert-are-here.component.html',
  styleUrls: ['./expert-are-here.component.scss']
})
export class ExpertAreHereComponent implements OnInit {
  expertData:Expert[] = initialExperts();
  isEditMode = false;

  editorConfig: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    height: '15rem',
    minHeight: '5rem',
    placeholder: 'Enter text here...',
    translate: 'no',
    defaultParagraphSeparator: 'p',
    defaultFontName: 'Arial',
    toolbarHiddenButtons: [
      ['bold']
      ],
    customClasses: [
      {
        name: "quote",
        class: "quote",
      },
      {
        name: 'redText',
        class: 'redText'
      },
      {
        name: "titleText",
        class: "titleText",
        tag: "h1",
      },
    ]
  };

  constructor() { }

  ngOnInit(): void {
  }

  update(){
    this.isEditMode = true;
  }
  add(){
    this.isEditMode = false;
  }

}
