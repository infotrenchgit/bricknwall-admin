import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExpertAreHereComponent } from './expert-are-here.component';

describe('ExpertAreHereComponent', () => {
  let component: ExpertAreHereComponent;
  let fixture: ComponentFixture<ExpertAreHereComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExpertAreHereComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExpertAreHereComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
