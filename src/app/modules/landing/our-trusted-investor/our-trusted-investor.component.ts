import { Component, OnInit } from '@angular/core';
import {TrustedInvestor,initialTrustedInvestors} from '../models/trustedInvestor.model';

@Component({
  selector: 'app-our-trusted-investor',
  templateUrl: './our-trusted-investor.component.html',
  styleUrls: ['./our-trusted-investor.component.scss']
})
export class OurTrustedInvestorComponent implements OnInit {

  trustedInvestors:TrustedInvestor[] = initialTrustedInvestors();
  isEditMode = false;

  constructor() { }

  ngOnInit(): void {
  }

  update(){
    this.isEditMode = true;
  }
  add(){
    this.isEditMode = false;
  }

}
