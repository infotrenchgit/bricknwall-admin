import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OurTrustedInvestorComponent } from './our-trusted-investor.component';

describe('OurTrustedInvestorComponent', () => {
  let component: OurTrustedInvestorComponent;
  let fixture: ComponentFixture<OurTrustedInvestorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OurTrustedInvestorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OurTrustedInvestorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
