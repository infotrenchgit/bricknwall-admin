import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LandingRoutingModule } from './landing-routing.module';
import { OneSolutionComponent } from './one-solution/one-solution.component';
import { CoreModule } from '../core/core.module';
import { SharedModule } from '../shared/shared.module';
import { ExpertAreHereComponent } from './expert-are-here/expert-are-here.component';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { OurSuggestionComponent } from './our-suggestion/our-suggestion.component';
import { CityComponent } from './city/city.component';
import { GrowTogetherComponent } from './grow-together/grow-together.component';
import { OurTrustedInvestorComponent } from './our-trusted-investor/our-trusted-investor.component';
import { SettingsComponent } from './settings/settings.component';


@NgModule({
  declarations: [OneSolutionComponent, ExpertAreHereComponent, OurSuggestionComponent, CityComponent, GrowTogetherComponent, OurTrustedInvestorComponent, SettingsComponent],
  imports: [
    CommonModule,
    LandingRoutingModule,
    CoreModule,
    SharedModule,
    AngularEditorModule
  ]
})
export class LandingModule { }
