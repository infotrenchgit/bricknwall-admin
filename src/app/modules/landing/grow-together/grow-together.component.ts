import { Component, OnInit } from '@angular/core';
import { initialGrowTogther, GrowTogther } from '../models/GrowTogether.model';
import { AngularEditorConfig } from '@kolkov/angular-editor';

@Component({
  selector: 'app-grow-together',
  templateUrl: './grow-together.component.html',
  styleUrls: ['./grow-together.component.scss']
})
export class GrowTogetherComponent implements OnInit {

  growTogetherContents:GrowTogther[] = initialGrowTogther();
  isEditMode = false;

  editorConfig: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    height: '15rem',
    minHeight: '5rem',
    placeholder: 'Enter text here...',
    translate: 'no',
    defaultParagraphSeparator: 'p',
    defaultFontName: 'Arial',
    toolbarHiddenButtons: [
      ['bold']
      ],
    customClasses: [
      {
        name: "quote",
        class: "quote",
      },
      {
        name: 'redText',
        class: 'redText'
      },
      {
        name: "titleText",
        class: "titleText",
        tag: "h1",
      },
    ]
  };

  constructor() { }

  ngOnInit(): void {
  }

  update(){
    this.isEditMode = true;
  }
  add(){
    this.isEditMode = false;
  }

}
