import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OneSolutionComponent } from './one-solution.component';

describe('OneSolutionComponent', () => {
  let component: OneSolutionComponent;
  let fixture: ComponentFixture<OneSolutionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OneSolutionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OneSolutionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
