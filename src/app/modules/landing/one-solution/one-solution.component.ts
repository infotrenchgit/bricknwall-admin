import { Component, OnInit } from '@angular/core';
import {OneSolution,initialOneSolution} from '../models/one-solution.model';

@Component({
  selector: 'app-one-solution',
  templateUrl: './one-solution.component.html',
  styleUrls: ['./one-solution.component.scss']
})
export class OneSolutionComponent implements OnInit {
  oneSolutionsdata:OneSolution[] = initialOneSolution();
  isEditMode = false;

  constructor() { }

  ngOnInit(): void {
  }

  update(){
    this.isEditMode = true;
  }
  add(){
    this.isEditMode = false;
  }

}
