import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OurSuggestionComponent } from './our-suggestion.component';

describe('OurSuggestionComponent', () => {
  let component: OurSuggestionComponent;
  let fixture: ComponentFixture<OurSuggestionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OurSuggestionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OurSuggestionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
