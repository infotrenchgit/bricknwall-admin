import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OneSolutionComponent } from './one-solution/one-solution.component';
import { ExpertAreHereComponent } from './expert-are-here/expert-are-here.component';
import { OurSuggestionComponent } from './our-suggestion/our-suggestion.component';
import { CityComponent } from './city/city.component';
import { GrowTogetherComponent } from './grow-together/grow-together.component';
import { OurTrustedInvestorComponent } from './our-trusted-investor/our-trusted-investor.component';
import { SettingsComponent } from './settings/settings.component';

const routes: Routes = [
  {path:'one-solution',component:OneSolutionComponent},
  {path:'expert-are-here',component:ExpertAreHereComponent},
  {path:'our-suggestion',component:OurSuggestionComponent},
  {path:'cities',component:CityComponent},
  {path:'grow-together',component:GrowTogetherComponent},
  {path:'our-trusted-investors',component:OurTrustedInvestorComponent},
  {path:'settings',component:SettingsComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LandingRoutingModule { }
