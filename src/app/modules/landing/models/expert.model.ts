
export type Expert = {
    id:number;
    icon:string;
    title:string;
    description?:string;
}

export function initialExperts():Expert[] {
    let experts:Expert[] = [];

    experts.push({id:1,icon:'https://thescpl.org/bricksnwall/assets/icons/Real-Estate.png?v=1.1',title:'Real Estate','description':'Guidance from our experts for the lifestyle-conscious / Guidance from our expert advisors for the lifestyle-conscious'});

    experts.push({id:2,icon:'https://thescpl.org/bricksnwall/assets/icons/Legal.png?v=1.1',title:'Legal',description:' Redefine your knowledge with our Legal Section '});

    experts.push({id:3,icon:'https://thescpl.org/bricksnwall/assets/icons/Investment.png?v=1.1',title:'Investment',description:'<ul _ngcontent-bcs-c42=""><li _ngcontent-bcs-c42="">We guide you for the best investment opportunities.</li><li _ngcontent-bcs-c42="">We guide your fears of investing.</li><li _ngcontent-bcs-c42="">We have the best options of Investment for you.</li></ul>'});

   

    return experts;

}