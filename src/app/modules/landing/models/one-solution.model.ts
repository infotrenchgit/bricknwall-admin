
export type OneSolution = {
    id:number;
    icon:string;
    title:string;
}

export function initialOneSolution():OneSolution[] {
    let oneSolutions:OneSolution[] = [];

    oneSolutions.push({id:1,icon:'https://thescpl.org/bricksnwall/assets/Home-Loan.png?v=1.1',title:'Home Loan'});

    oneSolutions.push({id:2,icon:'https://thescpl.org/bricksnwall/assets/Tie-ups-with-Reputed-Developers.png?v=1.1',title:'Tie-ups with Reputed Developers home loan'});

    oneSolutions.push({id:3,icon:'https://thescpl.org/bricksnwall/assets/Expreienced-Legal-Team.png?v=1.1',title:'Expreienced Legal Team'});

    oneSolutions.push({id:4,icon:'https://thescpl.org/bricksnwall/assets/End-to-End-Sercices.png?v=1.1',title:'End to End Services'});
    oneSolutions.push({id:5,icon:'https://thescpl.org/bricksnwall/assets/Dedicated-Personal.png?v=1.1',title:'Dedicated Personal'});

    return oneSolutions;

}