
export type TrustedInvestor = {
    id:number;
    logo:string;
    url:string;
}

export function initialTrustedInvestors():TrustedInvestor[] {
    let trustedInvestors:TrustedInvestor[] = [];

    trustedInvestors.push({id:1,logo:'http://thescpl.org/bricksnwall/assets/images/partner/dlf-india.png',url:'http://thescpl.org/bricksnwall/single-builder'});

    trustedInvestors.push({id:1,logo:'http://thescpl.org/bricksnwall/assets/images/partner/ats.png',url:'http://thescpl.org/bricksnwall/single-builder'});

    trustedInvestors.push({id:1,logo:'http://thescpl.org/bricksnwall/assets/images/partner/gogreg.png',url:'http://thescpl.org/bricksnwall/single-builder'});

    trustedInvestors.push({id:1,logo:'https://www.csipl.net/ae/images/clientlg-4.png',url:'http://thescpl.org/bricksnwall/single-builder'});

    trustedInvestors.push({id:1,logo:'https://www.csipl.net/ae/images/clientlg-5.png',url:'http://thescpl.org/bricksnwall/single-builder'});

    

    return trustedInvestors;

}