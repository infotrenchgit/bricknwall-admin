import { Component, OnInit } from '@angular/core';
import {Consultant,Legal,initialConsultant} from '../store/consultant.model';

@Component({
  selector: 'app-consultant-list',
  templateUrl: './consultant-list.component.html',
  styleUrls: ['./consultant-list.component.scss']
})
export class ConsultantListComponent implements OnInit {
  consultants:Consultant[] = initialConsultant();

  

  constructor() { }

  ngOnInit(): void {
  }

}
