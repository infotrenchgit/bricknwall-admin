import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsultantUpdateComponent } from './consultant-update.component';

describe('ConsultantUpdateComponent', () => {
  let component: ConsultantUpdateComponent;
  let fixture: ComponentFixture<ConsultantUpdateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConsultantUpdateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsultantUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
