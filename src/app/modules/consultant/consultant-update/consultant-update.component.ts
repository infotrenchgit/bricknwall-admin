import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-consultant-update',
  templateUrl: './consultant-update.component.html',
  styleUrls: ['./consultant-update.component.scss']
})
export class ConsultantUpdateComponent implements OnInit {

  privacyPolicy = {
    id:null,
    title:null,
    description:null,
    icons:[],
    banners:[]
  };
  isLegal:string = 'yes';
  isInvestment:string = 'yes';


  constructor() { }

  ngOnInit(): void {
  }

  update(f:NgForm,errorHTML:HTMLDivElement){}

  bannerImageUpload(e:any){
    this.privacyPolicy.banners = e;
  }
  iconImageUpload(e:any){
    this.privacyPolicy.icons = e;
  }

}
