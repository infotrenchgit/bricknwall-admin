import { Component, OnInit } from '@angular/core';
import {ConsultantProperty,consultantPropertyData} from '../store/consultant.model';
@Component({
  selector: 'app-consultant-property-list',
  templateUrl: './consultant-property-list.component.html',
  styleUrls: ['./consultant-property-list.component.scss']
})
export class ConsultantPropertyListComponent implements OnInit {
  consultantProperties:ConsultantProperty[] = consultantPropertyData;
  constructor() { }

  ngOnInit(): void {
  }

}
