import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsultantPropertyListComponent } from './consultant-property-list.component';

describe('ConsultantPropertyListComponent', () => {
  let component: ConsultantPropertyListComponent;
  let fixture: ComponentFixture<ConsultantPropertyListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConsultantPropertyListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsultantPropertyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
