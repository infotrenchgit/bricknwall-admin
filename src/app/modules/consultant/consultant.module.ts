import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ConsultantRoutingModule } from './consultant-routing.module';
import { ConsultantListComponent } from './consultant-list/consultant-list.component';
import { ConsultantUpdateComponent } from './consultant-update/consultant-update.component';
import { CoreModule } from '../core/core.module';
import { SharedModule } from '../shared/shared.module';
import { FormsModule } from '@angular/forms';
import { ConsultantPropertyListComponent } from './consultant-property-list/consultant-property-list.component';
import { ConsultantPropertyUpdateComponent } from './consultant-property-update/consultant-property-update.component';
import { AngularEditorModule } from '@kolkov/angular-editor';


@NgModule({
  declarations: [ConsultantListComponent, ConsultantUpdateComponent, ConsultantPropertyListComponent, ConsultantPropertyUpdateComponent],
  imports: [
    CommonModule,
    ConsultantRoutingModule,
    CoreModule,
    SharedModule,
    FormsModule,
    AngularEditorModule
  ]
})
export class ConsultantModule { }
