import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ConsultantListComponent } from './consultant-list/consultant-list.component';
import { ConsultantUpdateComponent } from './consultant-update/consultant-update.component';
import { ConsultantPropertyListComponent } from './consultant-property-list/consultant-property-list.component';
import { ConsultantPropertyUpdateComponent } from './consultant-property-update/consultant-property-update.component';

const routes: Routes = [
  {path:'list',component:ConsultantListComponent},
  {path:'add',component:ConsultantUpdateComponent},
  {path:'edit/:id',component:ConsultantUpdateComponent},
  {path:'property-list/:id',component:ConsultantPropertyListComponent},
  {path:'property-list-add/:id',component:ConsultantPropertyUpdateComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConsultantRoutingModule { }
