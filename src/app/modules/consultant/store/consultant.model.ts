export type Legal = {
    id:number;
    name:string;
    description:string;
}

export type Investment = {
    id:number;
    name:string;
    description:string;
}

export type Consultant = {
    id:number;
    name:string;
    email:string;
    phone:string;
    legal?:Legal[];
    inventments?:Investment[];
}




export interface ConsultantProperty{
    id:number;
    title: String;
    description:string;
    images:String[];
    location:string;
    isFeature:boolean;
    type:number;
    publishDate:Date;
    consultantId?:number,
    //owner:PropertyOwner;

};





export class PropertyModel{

    constructor(public property: ConsultantProperty){

    }

}

export function initialConsultant(){
    let consultants:Consultant[] = [];
    for(let i=1; i < 10; i ++){
        consultants.push(<Consultant>{
                id: i,
                name: 'Consultant ' + i,
                email: 'consultanttest' + i + '@gmail.com',
                phone:'9999999'
                
            });
    }
    return consultants;

}
export const consultantPropertyData:ConsultantProperty[] = [
    {
        id:1,
        title: 'Diamond Manor Apartment',
        description: 'Massa tempor nec feugiat nisl pretium. Egestas fringilla phasellus faucibus scelerisque eleifend donec Porta nibh venenatis cras sed felis eget velit aliquet. Neque volutpat ac tincidunt vitae semper quis lectus. Turpis in eu mi bibendum neque egestas congue quisque. Sed elementum tempus egestas sed sed risus pretium quam. Dignissim sodales ut eu sem. Nibh mauris cursus m',
        images:['https://maelectrics.com/tf/html/quarter-preview/quarter/img/img-slide/32.jpg','https://maelectrics.com/tf/html/quarter-preview/quarter/img/img-slide/31.jpg','https://maelectrics.com/tf/html/quarter-preview/quarter/img/img-slide/35.jpg','https://maelectrics.com/tf/html/quarter-preview/quarter/img/img-slide/34.jpg','https://maelectrics.com/tf/html/quarter-preview/quarter/img/img-slide/33.jpg'],
        location:'Belmont Gardens, Chicago',
        isFeature:true,
        type:1,
        publishDate: new Date()
    },
    {
        id:2,
        title: 'Diamond Manor Apartment 2',
        description: 'Massa tempor nec feugiat nisl pretium. Egestas fringilla phasellus faucibus scelerisque eleifend donec Porta nibh venenatis cras sed felis eget velit aliquet. Neque volutpat ac tincidunt vitae semper quis lectus. Turpis in eu mi bibendum neque egestas congue quisque. Sed elementum tempus egestas sed sed risus pretium quam. Dignissim sodales ut eu sem. Nibh mauris cursus m',
        images:['https://maelectrics.com/tf/html/quarter-preview/quarter/img/img-slide/32.jpg','https://maelectrics.com/tf/html/quarter-preview/quarter/img/img-slide/31.jpg','https://maelectrics.com/tf/html/quarter-preview/quarter/img/img-slide/35.jpg','https://maelectrics.com/tf/html/quarter-preview/quarter/img/img-slide/34.jpg','https://maelectrics.com/tf/html/quarter-preview/quarter/img/img-slide/33.jpg'],
        location:'Belmont Gardens, Chicago',
        isFeature:false,
        type:1,
        publishDate: new Date()
    },
    {
        id:3,
        title: 'Diamond Manor Apartment',
        description: 'Massa tempor nec feugiat nisl pretium. Egestas fringilla phasellus faucibus scelerisque eleifend donec Porta nibh venenatis cras sed felis eget velit aliquet. Neque volutpat ac tincidunt vitae semper quis lectus. Turpis in eu mi bibendum neque egestas congue quisque. Sed elementum tempus egestas sed sed risus pretium quam. Dignissim sodales ut eu sem. Nibh mauris cursus m',
        images:['https://maelectrics.com/tf/html/quarter-preview/quarter/img/img-slide/32.jpg','https://maelectrics.com/tf/html/quarter-preview/quarter/img/img-slide/31.jpg','https://maelectrics.com/tf/html/quarter-preview/quarter/img/img-slide/35.jpg','https://maelectrics.com/tf/html/quarter-preview/quarter/img/img-slide/34.jpg','https://maelectrics.com/tf/html/quarter-preview/quarter/img/img-slide/33.jpg'],
        location:'Belmont Gardens, Chicago',
        isFeature:true,
        type:1,
        publishDate: new Date()
    },
    {
        id:4,
        title: 'Diamond Manor Apartment',
        description: 'Massa tempor nec feugiat nisl pretium. Egestas fringilla phasellus faucibus scelerisque eleifend donec Porta nibh venenatis cras sed felis eget velit aliquet. Neque volutpat ac tincidunt vitae semper quis lectus. Turpis in eu mi bibendum neque egestas congue quisque. Sed elementum tempus egestas sed sed risus pretium quam. Dignissim sodales ut eu sem. Nibh mauris cursus m',
        images:['https://maelectrics.com/tf/html/quarter-preview/quarter/img/img-slide/32.jpg','https://maelectrics.com/tf/html/quarter-preview/quarter/img/img-slide/31.jpg','https://maelectrics.com/tf/html/quarter-preview/quarter/img/img-slide/35.jpg','https://maelectrics.com/tf/html/quarter-preview/quarter/img/img-slide/34.jpg','https://maelectrics.com/tf/html/quarter-preview/quarter/img/img-slide/33.jpg'],
        location:'Belmont Gardens, Chicago',
        isFeature:true,
        type:1,
        publishDate: new Date()
    },
    {
        id:5,
        title: 'Diamond Manor Apartment',
        description: 'Massa tempor nec feugiat nisl pretium. Egestas fringilla phasellus faucibus scelerisque eleifend donec Porta nibh venenatis cras sed felis eget velit aliquet. Neque volutpat ac tincidunt vitae semper quis lectus. Turpis in eu mi bibendum neque egestas congue quisque. Sed elementum tempus egestas sed sed risus pretium quam. Dignissim sodales ut eu sem. Nibh mauris cursus m',
        images:['https://maelectrics.com/tf/html/quarter-preview/quarter/img/img-slide/32.jpg','https://maelectrics.com/tf/html/quarter-preview/quarter/img/img-slide/31.jpg','https://maelectrics.com/tf/html/quarter-preview/quarter/img/img-slide/35.jpg','https://maelectrics.com/tf/html/quarter-preview/quarter/img/img-slide/34.jpg','https://maelectrics.com/tf/html/quarter-preview/quarter/img/img-slide/33.jpg'],
        location:'Belmont Gardens, Chicago',
        isFeature:true,
        type:1,
        publishDate: new Date()
    },
    {
        id:6,
        title: 'Diamond Manor Apartment',
        description: 'Massa tempor nec feugiat nisl pretium. Egestas fringilla phasellus faucibus scelerisque eleifend donec Porta nibh venenatis cras sed felis eget velit aliquet. Neque volutpat ac tincidunt vitae semper quis lectus. Turpis in eu mi bibendum neque egestas congue quisque. Sed elementum tempus egestas sed sed risus pretium quam. Dignissim sodales ut eu sem. Nibh mauris cursus m',
        images:['https://maelectrics.com/tf/html/quarter-preview/quarter/img/img-slide/32.jpg','https://maelectrics.com/tf/html/quarter-preview/quarter/img/img-slide/31.jpg','https://maelectrics.com/tf/html/quarter-preview/quarter/img/img-slide/35.jpg','https://maelectrics.com/tf/html/quarter-preview/quarter/img/img-slide/34.jpg','https://maelectrics.com/tf/html/quarter-preview/quarter/img/img-slide/33.jpg'],
        location:'Belmont Gardens, Chicago',
        isFeature:true,
        type:1,
        publishDate: new Date()
    },
    {
        id:7,
        title: 'Diamond Manor Apartment',
        description: 'Massa tempor nec feugiat nisl pretium. Egestas fringilla phasellus faucibus scelerisque eleifend donec Porta nibh venenatis cras sed felis eget velit aliquet. Neque volutpat ac tincidunt vitae semper quis lectus. Turpis in eu mi bibendum neque egestas congue quisque. Sed elementum tempus egestas sed sed risus pretium quam. Dignissim sodales ut eu sem. Nibh mauris cursus m',
        images:['https://maelectrics.com/tf/html/quarter-preview/quarter/img/img-slide/32.jpg','https://maelectrics.com/tf/html/quarter-preview/quarter/img/img-slide/31.jpg','https://maelectrics.com/tf/html/quarter-preview/quarter/img/img-slide/35.jpg','https://maelectrics.com/tf/html/quarter-preview/quarter/img/img-slide/34.jpg','https://maelectrics.com/tf/html/quarter-preview/quarter/img/img-slide/33.jpg'],
        location:'Belmont Gardens, Chicago',
        isFeature:true,
        type:1,
        publishDate: new Date()
    },
    {
        id:8,
        title: 'Diamond Manor Apartment',
        description: 'Massa tempor nec feugiat nisl pretium. Egestas fringilla phasellus faucibus scelerisque eleifend donec Porta nibh venenatis cras sed felis eget velit aliquet. Neque volutpat ac tincidunt vitae semper quis lectus. Turpis in eu mi bibendum neque egestas congue quisque. Sed elementum tempus egestas sed sed risus pretium quam. Dignissim sodales ut eu sem. Nibh mauris cursus m',
        images:['https://maelectrics.com/tf/html/quarter-preview/quarter/img/img-slide/32.jpg','https://maelectrics.com/tf/html/quarter-preview/quarter/img/img-slide/31.jpg','https://maelectrics.com/tf/html/quarter-preview/quarter/img/img-slide/35.jpg','https://maelectrics.com/tf/html/quarter-preview/quarter/img/img-slide/34.jpg','https://maelectrics.com/tf/html/quarter-preview/quarter/img/img-slide/33.jpg'],
        location:'Belmont Gardens, Chicago',
        isFeature:true,
        type:1,
        publishDate: new Date()
    },



];

