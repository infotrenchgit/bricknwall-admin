import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsultantPropertyUpdateComponent } from './consultant-property-update.component';

describe('ConsultantPropertyUpdateComponent', () => {
  let component: ConsultantPropertyUpdateComponent;
  let fixture: ComponentFixture<ConsultantPropertyUpdateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConsultantPropertyUpdateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsultantPropertyUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
