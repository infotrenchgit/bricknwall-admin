import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SideMenuComponent } from './side-menu/side-menu.component';
import { TopNavBarComponent } from './top-nav-bar/top-nav-bar.component';
import { ThemeSettingComponent } from './theme-setting/theme-setting.component';
import { RightSideBarComponent } from './right-side-bar/right-side-bar.component';
import { RouterModule } from '@angular/router';
import { StripHtmlPipe } from './pipes/strip-html.pipe';
import { SortPipe } from './pipes/sort.pipe';



@NgModule({
  declarations: [SideMenuComponent, TopNavBarComponent, ThemeSettingComponent, RightSideBarComponent, StripHtmlPipe, SortPipe],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports:[
    SideMenuComponent,
    TopNavBarComponent,
    ThemeSettingComponent,
    RightSideBarComponent,
    StripHtmlPipe
  ]
})
export class SharedModule { }
