import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class DataService {

    // allPassedData: BehaviorSubject<any> = new BehaviorSubject<any>([]);
    // constructor() { }

    // storePassedObject(passedData: any) {
    //     this.allPassedData.next(passedData);
    // }
    // // here instead of retrieve like this you can directly subscribe the property in your components
    // retrievePassedObject() {
    //     return this.allPassedData;

    // }

    private paramSource = new BehaviorSubject(null);
    sharedParam = this.paramSource.asObservable();

    constructor() { }

    changeParam(param: any) {
        console.log('SENDING IMAGES:', param);
        this.paramSource.next(param)
    }
}