
export type Blog = {
    id:number;
    title:string;
    description:string;
    categories:string[];
    tags?:string[];
    date?:string;
    author:string;
    comments:string[];
}

export type Career = {
    id:number;
    designation:string;
    description:string;
    location:string;
    categories:string[];
    date?:string;
    duration: string;
}



export function initialCareers<T>():T[]{
    let careers:T[] = [];
    careers.push(<T><unknown>{
        id:1,
        designation:'Graphic Designer',
        description:'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. ',
        location:'Noida',
        categories:['Category A','Category B'],
        date: new Date(),
        duration: 'Full Time'
    });
    careers.push(<T><unknown>{
        id:1,
        designation:'Graphic Designer',
        description:'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. ',
        location:'Noida',
        categories:['Category A','Category B'],
        date: new Date(),
        duration: 'Full Time'
    });
    careers.push(<T><unknown>{
        id:1,
        designation:'Graphic Designer',
        description:'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. ',
        location:'Noida',
        categories:['Category A','Category B'],
        date: new Date(),
        duration: 'Full Time'
    });
    careers.push(<T><unknown>{
        id:1,
        designation:'Graphic Designer',
        description:'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. ',
        location:'Noida',
        categories:['Category A','Category B'],
        date: new Date(),
        duration: 'Full Time'
    });
    careers.push(<T><unknown>{
        id:1,
        designation:'Graphic Designer',
        description:'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. ',
        location:'Noida',
        categories:['Category A','Category B'],
        date: new Date(),
        duration: 'Full Time'
    });
    careers.push(<T><unknown>{
        id:1,
        designation:'Graphic Designer',
        description:'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. ',
        location:'Noida',
        categories:['Category A','Category B'],
        date: new Date(),
        duration: 'Full Time'
    });
    careers.push(<T><unknown>{
        id:1,
        designation:'Graphic Designer',
        description:'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. ',
        location:'Noida',
        categories:['Category A','Category B'],
        date: new Date(),
        duration: 'Full Time'
    });
    careers.push(<T><unknown>{
        id:1,
        designation:'Graphic Designer',
        description:'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. ',
        location:'Noida',
        categories:['Category A','Category B'],
        date: new Date(),
        duration: 'Full Time'
    });
    careers.push(<T><unknown>{
        id:1,
        designation:'Graphic Designer',
        description:'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. ',
        location:'Noida',
        categories:['Category A','Category B'],
        date: new Date(),
        duration: 'Full Time'
    });
    careers.push(<T><unknown>{
        id:1,
        designation:'Graphic Designer',
        description:'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. ',
        location:'Noida',
        categories:['Category A','Category B'],
        date: new Date(),
        duration: 'Full Time'
    });
    careers.push(<T><unknown>{
        id:1,
        designation:'Graphic Designer',
        description:'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. ',
        location:'Noida',
        categories:['Category A','Category B'],
        date: new Date(),
        duration: 'Full Time'
    });
    careers.push(<T><unknown>{
        id:1,
        designation:'Graphic Designer',
        description:'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. ',
        location:'Noida',
        categories:['Category A','Category B'],
        date: new Date(),
        duration: 'Full Time'
    });
    
    return careers;

}

