import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CareerRoutingModule } from './career-routing.module';
import { UpdateCareerComponent } from './update-career/update-career.component';
import { ListCareerComponent } from './list-career/list-career.component';
import { SharedModule } from '../shared/shared.module';
import { CoreModule } from '../core/core.module';
import { FormsModule } from '@angular/forms';
import { AngularEditorModule } from '@kolkov/angular-editor';


@NgModule({
  declarations: [ UpdateCareerComponent, ListCareerComponent],
  imports: [
    CommonModule,
    CareerRoutingModule,
    SharedModule,
    CoreModule,
    FormsModule,
    AngularEditorModule
  ]
})
export class CareerModule { }
