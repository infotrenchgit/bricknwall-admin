import { Component, OnInit } from '@angular/core';
import {Career,initialCareers} from '../models/career.model';

@Component({
  selector: 'app-list-career',
  templateUrl: './list-career.component.html',
  styleUrls: ['./list-career.component.scss']
})
export class ListCareerComponent implements OnInit {
  careers:Career[] = initialCareers();

  constructor() { }

  ngOnInit(): void {
  }

}
