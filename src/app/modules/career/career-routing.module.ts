import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListCareerComponent } from './list-career/list-career.component';
import { UpdateCareerComponent } from './update-career/update-career.component';

const routes: Routes = [
  {path:'list',component:ListCareerComponent},
  {path:'add',component:UpdateCareerComponent},
  {path:'edit/:id',component:UpdateCareerComponent},
  {path:'',redirectTo:'list'},

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CareerRoutingModule { }
