import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'user',
    loadChildren: () => import('./modules/user/user.module').then(m => m.UserModule),
  },
  {
    path: 'cms',
    loadChildren: () => import('./modules/cms/cms.module').then(m => m.CmsModule),
  },
  {
    path: 'property',
    loadChildren: () => import('./modules/property/property.module').then(m => m.PropertyModule),
  },
  {
    path: 'broker',
    loadChildren: () => import('./modules/broker/broker.module').then(m => m.BrokerModule),
  },
  {
    path: 'blog',
    loadChildren: () => import('./modules/blog/blog.module').then(m => m.BlogModule),
  },
  {
    path: 'consultant',
    loadChildren: () => import('./modules/consultant/consultant.module').then(m => m.ConsultantModule),
  },
  {
    path: 'team',
    loadChildren: () => import('./modules/team/team.module').then(m => m.TeamModule),
  },
  {
    path: 'testimonial',
    loadChildren: () => import('./modules/testimonial/testimonial.module').then(m => m.TestimonialModule),
  },

  {
    path: 'faq',
    loadChildren: () => import('./modules/faq/faq.module').then(m => m.FaqModule),
  },

  {
    path: 'nri',
    loadChildren: () => import('./modules/nri/nri.module').then(m => m.NriModule),
  },

  {
    path: 'landing',
    loadChildren: () => import('./modules/landing/landing.module').then(m => m.LandingModule),
  },
  {
    path: 'career',
    loadChildren: () => import('./modules/career/career.module').then(m => m.CareerModule),
  },


  {
    path: '',
    redirectTo: '/user/login',
    pathMatch: 'full'
  }


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
